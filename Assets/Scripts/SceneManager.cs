﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneManager : MonoBehaviour
{
    public static SceneManager _sceneManager;


    public MinionManager minionManager;
    public UIController uiController;
    public CameraController cameraController;
    public InputController inputController;


    private int minionDeathCount;
    private int minionDeathLimit = 40;


    private void Awake()
    {
        _sceneManager = this;
    }

    public static SceneManager GetManager()
    {
        return _sceneManager;
    }

    void Start()
    {
        
    }
    public void StartGameplay()
    {
        minionManager.StartGameplay();
    }

    public void EndLevel()
    {
        // call at level end
        minionManager.StopMinionSpwan();
        StopAllMinions();        
        uiController.ShowEndPanel();
    }
    public void MinionKilled()
    {
        minionDeathCount++;
        CheckEndLevel();
    }
    void CheckEndLevel()
    {
        if (minionDeathCount >= minionDeathLimit)
        {
            // end level
            EndLevel();
        }
    }

    void StopAllMinions()
    {
        MinionController[] minions = FindObjectsOfType(typeof(MinionController)) as MinionController[];
        foreach (MinionController minis in minions)
        {
            minis.StopAll();
        }
    }

    public void CameraRotationOn(bool isOn)
    {
        cameraController.CameraRotationStatus(isOn);
    }
}
