﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireCube : MonoBehaviour
{
    public bool isDestroyable = true;
    public float lifeTime = 5f;
    private WaitForSeconds WaitOne;
    void Start()
    {
        WaitOne = new WaitForSeconds(lifeTime);
        StartCoroutine(LifeTimeCounter());
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("minion"))
        {
            other.transform.GetComponent<MinionController>().DestroyThySelf();
        }
    }

    IEnumerator LifeTimeCounter()
    {
        yield return WaitOne;
        StopAllCoroutines();
        if (isDestroyable)
        {
            Destroy(gameObject);
        }
        
    }
}
