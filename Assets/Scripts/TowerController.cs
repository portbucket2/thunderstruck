﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerController : MonoBehaviour
{


    public int totalPoint = 100;
    public int interval = 10;
    public ParticleSystem effect01;

    public GameObject[] towerLevels;

    private int totalTowerLevels;
    private int towerLevelCount;

    private int pointCounter = 0;
    private int devisable = 0;

    SceneManager sceneManager;

    private void Awake()
    {
        totalTowerLevels = towerLevels.Length;
        towerLevelCount = 0;
    }

    void Start()
    {
        devisable = totalPoint / interval;
        sceneManager = SceneManager.GetManager();
    }



    void IncreaseTowerlevel()
    {
        towerLevelCount++;
        UpdateTowerCondition();
        effect01.Play();
    }

    void DecreaseTowerLevel()
    {
        towerLevelCount--;
        UpdateTowerCondition();
    }
    void UpdateTowerCondition()
    {
        for (int i = 0; i < totalTowerLevels; i++)
        {
            towerLevels[i].SetActive(false);
        }
        int max = towerLevelCount;
        for (int i = 0; i < max; i++)
        {
            towerLevels[i].SetActive(true);
        }
        CheckLevelEnd();
        sceneManager.uiController.AddPointTopBar(-0.1f);
    }
    void CheckLevelEnd()
    {
        if (towerLevelCount >= totalTowerLevels )
        {
            // level end
            Debug.Log("level failed and ended");
            sceneManager.EndLevel();
            // reset everything
        }


    }
    public void AddPointToTower()
    {
        pointCounter++;
        CheckTowerUpgrade();
    }
    void CheckTowerUpgrade()
    {
        if (pointCounter > 0 && (pointCounter % devisable) == 0) 
        {
            // change level
            IncreaseTowerlevel();
        }
        else
        {
            // do nothing
        }
    }

}
