﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public bool isCameraRotating = true;
    public float rotationSpeed;
    private Vector3 tempRot;
    private float tempAxisX;
    private float tempAxisY;

    private float dis;
    private float disY;

    private Vector2 mouseOldPos;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            tempAxisX = 0;
            tempAxisY = 0;
            mouseOldPos = Input.mousePosition;
        }
        if (Input.GetMouseButton(0))
        {
            if (isCameraRotating)
            {
                tempAxisX = Input.GetAxis("Mouse X") * 300f;
                tempAxisY = Input.GetAxis("Mouse Y");
                if (Mathf.Abs(tempAxisX) > 0.5)
                {
                    dis = Mathf.Abs(Input.mousePosition.x - mouseOldPos.x);
                    disY = Mathf.Abs(Input.mousePosition.y - mouseOldPos.y);
                    if (tempAxisX > 0)
                    {
                        //pos
                        RotateCameraAround(dis);
                    }
                    else
                    {
                        // neg
                        RotateCameraAround(-dis);
                    }
                }
            }
        }
    }

    void RotateCameraAround(float Ycel)
    {
        tempRot = new Vector3(0f, Ycel * Time.deltaTime * rotationSpeed, 0f);
        transform.Rotate(tempRot, Space.World);
    }
    public void CameraRotationStatus(bool isOn)
    {
        isCameraRotating = isOn;
    }
}
