﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinionManager : MonoBehaviour
{
    public bool isSpwaning = true;

    public MinionController minionPrefab;

    public int startSpwanCount;
    public float spwanInterval;
    public float spwanPositionRadius;

    public Transform tower;
    public List<Transform> motherBoxes;


    private WaitForSeconds WaitSeconds;
     
    void Start()
    {
        WaitSeconds = new WaitForSeconds(spwanInterval);
        //StartGameplay();
    }
    public void StartGameplay()
    {
        int max = startSpwanCount;
        int totalBox = motherBoxes.Count;
        for (int i = 0; i < max; i++)
        {
            SpwanMinions(motherBoxes[Random.Range(0, totalBox)]);
        }

        StartCoroutine(MinionSpwanRoutine());
    }

    void SpwanMinions(Transform m_motherBox)
    {
        Vector2 rando = Random.insideUnitCircle * spwanPositionRadius;
        Vector3 newPos = m_motherBox.position + new Vector3(rando.x, 0, rando.y);
        MinionController gg = Instantiate(minionPrefab, newPos, Quaternion.identity );
        gg.SetMinion(m_motherBox, tower);
    }

    IEnumerator MinionSpwanRoutine()
    {
        int totalBox = motherBoxes.Count;
        while (isSpwaning)
        {
            yield return WaitSeconds;
            SpwanMinions(motherBoxes[Random.Range(0, totalBox)]);
        }
    }

    public void StopMinionSpwan()
    {
        StopAllCoroutines();
        isSpwaning = false;
    }
}
