﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class UIController : MonoBehaviour
{
    
    public Button startGameButton;
    public GameObject startpanel;
    public Button nextLevelButton;
    public GameObject endpanel;
    public Image topBarImage;
    public float barFillSpeed = 1f;
    public Button thunderButton;
    public Image thunderCoolDown;
    public Button fireButton;
    public Image fireCoolDown;
    public GameObject powerSelectionPanel;

    private float topBarPoint = 0.5f;
    WaitForEndOfFrame ENDOFFRAME = new WaitForEndOfFrame();
    SceneManager sceneManager;
    void Start()
    {
        sceneManager = SceneManager.GetManager();

        ButtonsInit();
    }



    void ButtonsInit()
    {
        startGameButton.onClick.AddListener(delegate { StartGameplay(); });
        nextLevelButton.onClick.AddListener(delegate { GotoNextLevel(); });
        thunderButton.onClick.AddListener(delegate { Thunder(); });
        fireButton.onClick.AddListener(delegate { Fire(); });
    }


    void StartGameplay()
    {
        startpanel.SetActive(false);
        sceneManager.StartGameplay();
        PowerSelectionPanelOn();
    }
    void GotoNextLevel()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("GameplayScene");
    }
    public void ShowEndPanel()
    {
        endpanel.SetActive(true);
    }

    
    public void AddPointTopBar(float point) { topBarPoint += point; TopBarCalculation(); }

    void TopBarCalculation()
    {
        //StopAllCoroutines();
        StartCoroutine(SmoothFill());
    }
    IEnumerator SmoothFill()
    {
        float step = 0;
        float prev = topBarImage.fillAmount;
        while (step < 1) 
        {
            topBarImage.fillAmount = Mathf.Lerp(prev, topBarPoint, step);
            step += Time.deltaTime * barFillSpeed;
            yield return ENDOFFRAME;
        }
        topBarImage.fillAmount = topBarPoint;
    }
    void Thunder()
    {
        sceneManager.inputController.EnableLightningEffect();
        StartCoroutine(ThunderButtonCoolDown());
    }
    void Fire()
    {
        /*stop camera move
         * on press start timer
         * let draw
         * stop fire on timer end
         * eneable camera
         */
        sceneManager.inputController.EnableFireEffect();
        FireButtonEnabled(false);
        StartCoroutine(FireButtonCoolDown());
    }
    public void FireButtonEnabled(bool isOn)
    {
        fireButton.interactable = isOn;
    }
    public void ThunderButtonEnabled(bool isOn)
    {
        thunderButton.interactable = isOn;
    }

    IEnumerator FireButtonCoolDown()
    {
        float duration = 5f;
        float total = duration;
        float value = 0;
        while (duration > 0) 
        {
            value = duration / total;
            fireCoolDown.fillAmount = 1f - value;
            duration -= Time.deltaTime;
            yield return ENDOFFRAME;
        }
        yield return ENDOFFRAME;
        fireCoolDown.fillAmount = 1;
        FireButtonEnabled(true);
    }
    IEnumerator ThunderButtonCoolDown()
    {
        ThunderButtonEnabled(false);
        float duration = 5f;
        float total = duration;
        float value = 0;
        while (duration > 0)
        {
            value = duration / total;
            thunderCoolDown.fillAmount =1f- value;
            duration -= Time.deltaTime;
            yield return ENDOFFRAME;
        }
        yield return ENDOFFRAME;
        thunderCoolDown.fillAmount = 1;
        ThunderButtonEnabled(true);
    }
    void PowerSelectionPanelOn()
    {
        powerSelectionPanel.SetActive(true);
    }
}
