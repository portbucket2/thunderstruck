﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointerObject : MonoBehaviour
{
    public ParticleSystem effect01;
    public ParticleSystem effect02;
    public DigitalRuby.LightningBolt.LightningBoltScript thunderScript;

    void Start()
    {
        
    }

    public void SetCurrentLocation( Vector3 loc)
    {
        transform.position = new Vector3(loc.x, 0.5f,loc.z);
    }

    public void StartEffects()
    {
        effect01.Play();
    }
    public void EndEffects()
    {
        effect01.Stop();
    }

    public void ThunderStrike()
    {
        thunderScript.ThunderStrike();
        effect02.Play();
    }
}
