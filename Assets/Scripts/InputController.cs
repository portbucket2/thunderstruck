﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour
{
    public Camera mainCam;
    public GameObject lightningStrike;
    public GameObject fireCube;
    public GameObject lightningCube;
    public PointerObject pointerObject;

    readonly string MINION = "minion";
    private Vector3 fireLineStart;

    private bool fireEnabled = false;
    private bool lightningEnabled = false;

    private WaitForEndOfFrame ENDOFFRAME = new WaitForEndOfFrame();

    SceneManager sceneManager;
    void Start()
    {
        sceneManager = SceneManager.GetManager();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            fireLineStart = Vector3.zero;
            if (fireEnabled)
            {
                StartCoroutine(FireEffectCountDown());
                pointerObject.ThunderStrike();
                pointerObject.StartEffects();
            }
            if (lightningEnabled)
            {
                SpwanLightningCube();
            }
        }
        if (Input.GetMouseButton(0))
        {
            if (fireEnabled)
            {
                DrawFireLine();
            }
        }
        if (Input.GetMouseButtonUp(0))
        {
            RaycastHit hit;
            Ray ray = mainCam.ScreenPointToRay(Input.mousePosition);
            
            if (Physics.Raycast(ray, out hit))
            {
                //Debug.DrawLine(mainCam.transform.position, hit.point, Color.red, 10);
                if (hit.transform.gameObject.CompareTag(MINION))
                {
                    //Debug.Log("hit minion");
                    lightningStrike.transform.position = hit.transform.position;
                    lightningStrike.SetActive(false);
                    lightningStrike.SetActive(true);
                    MinionController m_minion = hit.transform.GetComponent<MinionController>();
                    m_minion.DestroyThySelf();
                }
                // Do something with the object that was hit by the raycast.
            }

            if (fireEnabled)
            {
                //fireEnabled = false;
                //sceneManager.CameraRotationOn(true);
                pointerObject.EndEffects();
            }
        }
    }

    void DrawFireLine()
    {
        RaycastHit hit;
        Ray ray = mainCam.ScreenPointToRay(Input.mousePosition);

        
        if (Physics.Raycast(ray, out hit))
        {
            if (fireLineStart == Vector3.zero)
            {
                fireLineStart = hit.point;
                //spwan firecube
                SpwanFireCube(hit.point);
            }
            else if (Vector3.Distance(fireLineStart, hit.point)>0.5f)
            {
                fireLineStart = hit.point;
                //spwan fire cube
                SpwanFireCube(hit.point);
            }
            pointerObject.SetCurrentLocation(hit.point);
        }
    }
    void SpwanFireCube(Vector3 pos)
    {
        Vector3 _pos = new Vector3(pos.x, 0.5f, pos.z);
        GameObject gg = Instantiate(fireCube,_pos, Quaternion.identity);
    }
    void SpwanLightningCube()
    {
        RaycastHit hit;
        Ray ray = mainCam.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit))
        {
            Vector3 _pos = new Vector3(hit.point.x, 0.5f, hit.point.z);
            GameObject gg = Instantiate(lightningCube, _pos, Quaternion.identity);
            lightningEnabled = false;
            pointerObject.SetCurrentLocation(hit.point);
            pointerObject.ThunderStrike();
        }
            
    }
    public void EnableLightningEffect()
    {
        lightningEnabled = true;
    }
    public void EnableFireEffect()
    {
        fireEnabled = true;
        sceneManager.CameraRotationOn(false);
    }
    IEnumerator FireEffectCountDown() {
        float timer = 1f;
        while (timer >0)
        {
            fireEnabled = true;
            timer -= Time.deltaTime;
            yield return ENDOFFRAME;
        }
        yield return ENDOFFRAME;
        fireEnabled = false;
        sceneManager.CameraRotationOn(true);
        pointerObject.EndEffects();
        //sceneManager.uiController.FireButtonEnabled(true);
    }
}
