﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MinionController : MonoBehaviour
{
    public Animator animator;
    public GameObject stone;

    [Header("Keep empty!!")]
    public Transform motherBox;
    public Transform targetTower;

    public float cargo = 0f;
    private NavMeshAgent agent;

    private readonly string TOWER = "tower";
    private readonly string MOTHERBOX = "motherBox";

    private readonly string IDLE = "idle";
    private readonly string WALK = "walk";
    private readonly string STONEWALK = "stoneWalk";
    private readonly string DEATH = "death";

    WaitForEndOfFrame ENDOFFRAME = new WaitForEndOfFrame();
    WaitForSeconds WAITONE = new WaitForSeconds(1.5f);

    private SceneManager sceneManager;

    private void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
    }
    void Start()
    {
        sceneManager = SceneManager.GetManager();
        //SelectNexttarget();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        //Debug.Log("Collided!!");
        if (other.gameObject.CompareTag(TOWER))
        {
            cargo = 0;
            SelectNexttarget();
            other.transform.GetComponent<TowerController>().AddPointToTower();
        }else if (other.gameObject.CompareTag(MOTHERBOX))
        {
            cargo = 1;
            SelectNexttarget();
        }
        else
        {
            // 
        }
    }
    public void SetMinion(Transform m_motherBox, Transform m_tower)
    {
        motherBox = m_motherBox;
        targetTower = m_tower;
        SelectNexttarget();
    }
    void SelectNexttarget()
    {
        if (cargo > 0)
        {
            // go to tower
            DeliverCargo();
            
        }
        else
        {
            // get cargo
            GetCargo();
        }
    }

    void GetCargo()
    {
        agent.SetDestination(motherBox.position);
        Walk();
    }

    void DeliverCargo()
    {
        agent.SetDestination(targetTower.position);
        StoneWalk();
    }

    public void DestroyThySelf()
    {
        // do animation before destory
        sceneManager.MinionKilled();
        StopAllCoroutines();
        StartCoroutine(DeathRoutine());
    }

    IEnumerator DeathRoutine()
    {
        sceneManager.uiController.AddPointTopBar(0.05f);
        agent.SetDestination(transform.position);
        Death();
        yield return WAITONE;
        Destroy(gameObject);
    }

    void Walk() { animator.SetTrigger(WALK); stone.SetActive(false); }
    void StoneWalk() { animator.SetTrigger(STONEWALK); stone.SetActive(true); }
    void Death() { animator.SetTrigger(DEATH); stone.SetActive(false); }


    public void StopAll()
    {
        agent.SetDestination(transform.position);
        agent.speed = 0;
        Death();
        //DestroyThySelf();
    }
}
